package org.npci.evolutionoftrust;

import java.util.Scanner;

public class ConsoleBehavior implements PlayerMoveBehaviour {

    private Scanner inputScannerObject;

    public ConsoleBehavior(Scanner inputScannerObject) {
        this.inputScannerObject = inputScannerObject;
    }

    @Override
    public MoveType getMove() {
        System.out.println("Choose behavior");
        System.out.println("Press 1 to Cheat");
        System.out.println("Press 2 to Cooperate");

        String input = inputScannerObject.nextLine();

        if(input.equals("1"))
            return MoveType.CHEAT;
        else
            return MoveType.COOPERATE;
    }
}
