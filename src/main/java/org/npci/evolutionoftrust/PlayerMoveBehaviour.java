package org.npci.evolutionoftrust;

public interface PlayerMoveBehaviour {

    PlayerMoveBehaviour ALWAYS_COOPERATE_BEHAVIOUR = () -> MoveType.COOPERATE;
    PlayerMoveBehaviour ALWAYS_CHEAT_BEHAVIOUR = () -> MoveType.CHEAT;

    MoveType getMove();
}
