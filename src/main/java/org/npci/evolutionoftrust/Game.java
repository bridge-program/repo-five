package org.npci.evolutionoftrust;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Game {
    private final Player one;
    private final Player two;
    private int numberOfRounds;
    private List<GameObserver> gameObserver;

   public Game(Player one, Player two, int numberOfRounds, GameObserver... gameObserver) {
        this.one = one;
        this.two = two;
        this.numberOfRounds = numberOfRounds;
        this.gameObserver = Arrays.asList(gameObserver);
    }

    public Score score() {
        RuleEngine ruleEngine;
        Score score;

        for(int i = 0; i < numberOfRounds; i++) {
            ruleEngine = new RuleEngine(one.getMove(), two.getMove());
            score = ruleEngine.score();
            one.setScoreFor(i + 1, score.getForPlayerOne());
            two.setScoreFor(i + 1, score.getForPlayerTwo());
            }

        Score totalScore = new Score(one.getTotalScore(), two.getTotalScore());
        System.out.println(totalScore.toString());

        gameObserver.forEach(GameObserver::gameEnded);
        return totalScore;
    }

    public static void main(String[] args) {
        Scanner p1sc = new Scanner(System.in);
        Scanner p2sc = new Scanner(System.in);
        Player playerOne = new Player("playerOne", new ConsoleBehavior(p1sc));
        Player playerTwo = new Player("playerTwo", new ConsoleBehavior(p2sc));
       // new ConsoleBehavior(System.in)
        new Game(playerOne, playerTwo, 1).score();
    }
}
