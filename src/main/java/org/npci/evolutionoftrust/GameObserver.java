package org.npci.evolutionoftrust;

public interface GameObserver {

    public void gameEnded();
}
