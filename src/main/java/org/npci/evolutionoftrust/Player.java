package org.npci.evolutionoftrust;

import java.util.HashMap;
import java.util.Map;

import static org.npci.evolutionoftrust.MoveType.COOPERATE;

public class Player {
    private final Map<Integer,Integer> scores = new HashMap<>();
    private PlayerMoveBehaviour playerMoveBehaviour;

    public int getTotalScore() {
        return totalScore;
    }

    private int totalScore;


    public Player(String name, PlayerMoveBehaviour playerMoveBehaviour) {
        this.playerMoveBehaviour = playerMoveBehaviour;
    }

    public int scoreAt(int roundNumber) {
        if(scores.containsKey(roundNumber)) {
            return scores.get(roundNumber);
        }
        throw new RoundNotPlayedYetException();
    }

    public MoveType getMove() {
        return playerMoveBehaviour.getMove();
    }

    public void setScoreFor(int roundNumber, int score) {
        this.scores.put(roundNumber, score);
        this.totalScore += score;
    }
}
