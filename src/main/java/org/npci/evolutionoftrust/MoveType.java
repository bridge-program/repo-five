package org.npci.evolutionoftrust;

public enum MoveType {
    COOPERATE, CHEAT, ALTERNATING
}
