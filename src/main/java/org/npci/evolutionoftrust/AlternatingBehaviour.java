package org.npci.evolutionoftrust;

public class AlternatingBehaviour implements PlayerMoveBehaviour {

    private MoveType moveType;
    public AlternatingBehaviour() {
        moveType = MoveType.COOPERATE;
    }
    @Override
    public MoveType getMove() {
        if(moveType == PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR.getMove()) {
            moveType = PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR.getMove();
        }else{
            moveType = PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR.getMove();
        }
        return moveType;
    }
}
