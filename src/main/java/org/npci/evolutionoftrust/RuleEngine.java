package org.npci.evolutionoftrust;

import static org.npci.evolutionoftrust.MoveType.CHEAT;
import static org.npci.evolutionoftrust.MoveType.COOPERATE;

public class RuleEngine {
    private final MoveType one;
    private final MoveType two;

    public RuleEngine(MoveType one, MoveType two) {
        this.one = one;
        this.two = two;
    }

    public Score score() {
        if(one.equals(CHEAT) && two.equals(COOPERATE)) {
            return new Score(3,-1);
        }
        if(one.equals(COOPERATE) && two.equals(CHEAT)) {
            return new Score(-1,3);
        }
        if(one.equals(CHEAT) && two.equals(CHEAT)) {
            return new Score(-1,-1);
        }
        return new Score(2,2);
    }
}
