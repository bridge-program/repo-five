package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.is;


class ConsoleBehaviorTest {


    @Test
    public void shouldGetCheatMoveWhenPassesCodeAsCh() {
        Scanner sc = new Scanner("1");
        MoveType move = new ConsoleBehavior(sc).getMove();
        assertThat(move, is(MoveType.CHEAT));
    }

    @Test
    public void shouldGetCooperateMoveWhenPassesCodeAsCo() {
        Scanner sc = new Scanner("2");
        MoveType move = new ConsoleBehavior(sc).getMove();
        assertThat(move, is(MoveType.COOPERATE));
    }
}