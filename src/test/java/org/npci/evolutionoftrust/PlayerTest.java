package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.npci.evolutionoftrust.MoveType.CHEAT;
import static org.npci.evolutionoftrust.MoveType.COOPERATE;

class PlayerTest {

    @Test
    void playerShouldAlwaysCorporate() {
        assertThat(new Player("a", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR).getMove(), is(COOPERATE));
    }

    @Test
    void shouldGiveScoreForTheRoundThatHasBeenPlayed() {
        Player player = new Player("a", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        player.setScoreFor(1, 0);

        assertThat(player.scoreAt(1), is(0));
    }

    @Test
    void shouldGiveScoreForAnyRoundThatHasBeenPlayed() {
        Player player = new Player("a", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        player.setScoreFor(1, 0);
        player.setScoreFor(2, 1);

        assertThat(player.scoreAt(1), is(0));
        assertThat(player.scoreAt(2), is(1));
    }

    @Test
    void shouldThrowRoundNotPlayedExceptionForAScoreForARoundThatHasNotBeenPlayedYet() {
        Player player = new Player("a", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        assertThrows(RoundNotPlayedYetException.class, () -> player.scoreAt(1));
    }

    @Test
    void checkIfFirstMoveOfAlternatingPlayerIsAlwaysCheat(){
        Player player1 = new Player("a", new AlternatingBehaviour());
        assertThat(player1.getMove(),is(CHEAT));

    }

    @Test
    void checkIfSecondMoveOfAlternatingPlayerIsAlwaysCheat(){
        Player player1 = new Player("a", new AlternatingBehaviour());
        player1.getMove();
        assertThat(player1.getMove(),is(COOPERATE));
    }

    @Test
    void checkIfThirdMoveOfAlternatingPlayerIsAlwaysCheat(){
        Player player1 = new Player("a", new AlternatingBehaviour());
        player1.getMove();
        player1.getMove();
        assertThat(player1.getMove(),is(CHEAT));
    }


}