package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Scanner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

public class GameTest {

    @Test
    void shouldBeAbleToPlayOneRound() {
        Player one = new Player("one", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(2,2)));
    }

    @Test
    void shouldBeAbleToPlayTwoRound() {
        Player one = new Player("one", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 2).score();

        assertThat(actualScore, is(new Score(4,4)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWhereOneAlwaysCheatsSecondAlwaysCooperate() {
        Player one = new Player("one",  PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Player two = new Player("two",  PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 2).score();

        assertThat(actualScore, is(new Score(6,-2)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWithPlayersWhereOneAlwaysCooperateSecondAlwaysCheat() {
        Player one = new Player("one",  PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Player two = new Player("two",  PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Score actualScore = new Game(one, two, 2).score();

        assertThat(actualScore, is(new Score(-2,6)));
    }

    @Test
    void shouldBeAbleToPlayOneRoundWhereOneAlwaysCheatSecondAlternateChoices() {
        Player one = new Player("one",  PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Player two = new Player("two",  new AlternatingBehaviour());
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(-1,-1)));
    }

    @Test
    void shouldBeAbleToPlayOneRoundWhereOneAlternateChoiceSecondAlwaysCheat() {
        Player one = new Player("one", new AlternatingBehaviour() );
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR );
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(-1,-1)));
    }

    @Test
    void shouldBeAbleToPlayOneRoundWhereOneCooperateAlwaysSecondAlternateChoice() {
        Player one = new Player("one",  PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Player two = new Player("two",  new AlternatingBehaviour());
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(-1,3)));
    }

    @Test
    void shouldBeAbleToPlayOneRoundWhereOneAlternateChoiceSecondAlwaysCooperate() {
        Player one = new Player("one",  new AlternatingBehaviour());
        Player two = new Player("two",  PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(3,-1)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWhereOneAlternateChoiceSecondAlwaysCooperate() {
        Player one = new Player("one",  new AlternatingBehaviour());
        Player two = new Player("two",  PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 2).score();

        assertThat(actualScore, is(new Score(3 +2 ,-1 +2)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWhereOneAlternateChoiceSecondAlwaysCheat() {
        Player one = new Player("one",  PlayerMoveBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Player two = new Player("two",  new AlternatingBehaviour());
        Score actualScore = new Game(one, two, 2).score();
        assertThat(actualScore, is(new Score(-1 + 3,-1 -1)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWhereOneAlternateChoiceSecondAlternate() {
        Player one = new Player("one",  new AlternatingBehaviour());
        Player two = new Player("two",  new AlternatingBehaviour());
        Score actualScore = new Game(one, two, 2).score();
        assertThat(actualScore, is(new Score(-1 +2,-1 +2)));
    }

    @Test
    void shouldBeAbleToPlayOneRoundWhereOneAlwaysCooperateSecondChooseFromConsole() {
        Scanner playerOneInput = new Scanner("2");
        Player one = new Player("one",  new ConsoleBehavior(playerOneInput));
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 1).score();
        assertThat(actualScore, is(new Score( 2, 2)));
    }

    @Test
    void shouldBeAbleToPlayTwoRoundWhereOneAlwaysCooperateSecondChooseFromConsole() {
        Scanner playerOneInput = new Scanner("2\n2");
        Player one = new Player("one",  new ConsoleBehavior(playerOneInput));
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Score actualScore = new Game(one, two, 1).score();
        assertThat(actualScore, is(new Score( 2, 2)));
    }

    @Test
    void shouldInformScoreBoardWhenGameEnds() {
        Player one = new Player("one", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Player two = new Player("two", PlayerMoveBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        GameObserver gameObserver = mock(GameObserver.class);
        verify(gameObserver, times(0)).gameEnded();
        Score actualScore = new Game(one, two, 2, gameObserver).score();
        verify(gameObserver, times(1)).gameEnded();
    }
}
